# Temperature and Humidity Data logger

- To monitor air temperature and relative humidity levels at all times
- THDL is a system to detect any temperature anomalies and to alert on time so preventive maintenance can happen.

Here is the Project Planning Sheet: [https://www.google.com/url?q=https://docs.google.com/spreadsheets/d/1ayq6BoayqRruMNtnFKO3ipDIQE3Sg529AXDOp_mnxx0/edit?usp%3Dsharing&sa=D&ust=1597855682599000&usg=AFQjCNFfvmIFfDtwsz-O4BIZAHLSN5Xx0g](https://www.google.com/url?q=https://docs.google.com/spreadsheets/d/1ayq6BoayqRruMNtnFKO3ipDIQE3Sg529AXDOp_mnxx0/edit?usp%3Dsharing&sa=D&ust=1597855682599000&usg=AFQjCNFfvmIFfDtwsz-O4BIZAHLSN5Xx0g)